package com.generator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.BitSet;

public class MyClass {

    static String PATH_USERNAMES = "F:\\extras\\freelancer\\usernames.txt";

    public static void main(String [] args) {

        Connection dbConnection;

        // initialize database
        dbConnection = initializeDatabase();

        // read and parse file then save to db
        try (BufferedReader br = new BufferedReader(new FileReader(PATH_USERNAMES)))
        {
            dbConnection.setAutoCommit(false); //transaction block start
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                saveToDatabase(sCurrentLine, dbConnection);
            }
            dbConnection.commit(); //transaction block end

            dbConnection.close();
            System.out.println("Closed Database");

        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static Connection initializeDatabase() {
        Connection conn = null;

        try {
            System.out.println("Opening database...");

            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:usernames.db");

            System.out.println("Opened database successfully");

        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        return conn;
    }

    private static void saveToDatabase(String username, Connection conn) throws SQLException {
        System.out.println(username);

        String tableName = String.valueOf(username.charAt(0));
        String name = null;
        int number = 0;

        // separate name from number
        String[] arr = username.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
        name = arr[0];
        if (arr.length > 1)
            number = Integer.parseInt(arr[1]);

        Statement stmt = conn.createStatement();
        String sql = "INSERT INTO " + tableName + " (name, number) VALUES ('" + name + "', '" + number + "')";
        stmt.executeUpdate(sql);

    }

    private static int countEntry(String tableName, Connection conn) throws SQLException {

        Statement s = conn.createStatement();
        ResultSet r = s.executeQuery("SELECT COUNT(*) AS rowcount FROM " + tableName);
        r.next();
        int count = r.getInt("rowcount") ;
        r.close() ;

        return count;
    }

    private static void testBitSet() {

        int[] arr = {0,1,2,3,4,5,6,7,8,9,20};
        BitSet bitset = new BitSet();
        for (int i = 0; i < arr.length; i++){
            bitset.set(arr[i]);
        }

        System.out.println(bitset.nextClearBit(0));
    }

}


