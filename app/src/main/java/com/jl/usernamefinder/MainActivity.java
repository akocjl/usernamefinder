package com.jl.usernamefinder;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.BitSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import database.DatabaseHelper;
import database.UsernameDatabase;
import helper.IO;


public class MainActivity extends ActionBarActivity {

    EditText emailText;
    TextView usernameView, elapsedTimeView;
    TextView firstTimeView;
    Button checkButton;
    ButteryProgressBar progressBar;
    UsernameDatabase mUsernameDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        usernameView = (TextView) findViewById(R.id.usernameView);
        elapsedTimeView = (TextView) findViewById(R.id.elapsedTimeView);
        emailText = (EditText) findViewById(R.id.emailText);
        checkButton = (Button) findViewById(R.id.checkButton);
        progressBar = (ButteryProgressBar) findViewById(R.id.progress);
        firstTimeView = (TextView) findViewById(R.id.firstTimeView);

        if (isFirstUsed()) {
            new DatabaseSetUp().execute();
        } else {
            mUsernameDatabase = new UsernameDatabase(this);
        }
    }

    private boolean isFirstUsed() {
        SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        return prefs.getBoolean("first", true);
    }

    public void checkName(View view){
        String email = emailText.getText().toString();

        if (email.isEmpty()) {
            showMessage("Enter email address");
        } else if (!isEmailValid(email)) {
            showMessage("Please enter a valid email address");
        } else {
            new CheckUsernameTask().execute();
        }
    }

    private class DatabaseSetUp extends AsyncTask<Void, Void, Boolean>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();
            firstTimeView.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            DatabaseHelper helper = DatabaseHelper.getInstance(MainActivity.this);
            helper.getReadableDatabase();
            helper.close();

            // copy preloaded database from assets to internal database directory
            try {
                IO.copyDatabaseFromAssets("usernames.db", MainActivity.this);
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);

            if (success) {
                mUsernameDatabase = new UsernameDatabase(MainActivity.this);

                // set the flag to false, indicates that the first use setup is successful
                SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
                editor.putBoolean("first", false);
                editor.commit();

            }

            hideLoading();
            firstTimeView.setVisibility(View.INVISIBLE);
        }
    }

    private class CheckUsernameTask extends AsyncTask<Void, Void, String>{

        private long mStartTime;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mStartTime = System.nanoTime();
            showLoading();
            usernameView.setText("");
            elapsedTimeView.setText("");
            usernameView.setVisibility(View.INVISIBLE);
        }

        @Override
        protected String doInBackground(Void... voids) {
            String email = emailText.getText().toString().toLowerCase();
            String rawName = email.substring(0, email.indexOf("@"));
            String name = rawName.replaceAll("\\d", "");
            String recommendedUsername = name;

            BitSet bitSet = new BitSet();
            Cursor cursor = mUsernameDatabase.getUsernames(name);
            while (cursor.moveToNext()) {
                bitSet.set(cursor.getInt(0));
            }
            cursor.close();

            int next = bitSet.nextClearBit(0);
            if (next > 0)
                recommendedUsername = recommendedUsername + next;

            return recommendedUsername;
        }

        @Override
        protected void onPostExecute(String username) {
            super.onPostExecute(username);
            hideLoading();

            double elapsedTime = (System.nanoTime() - mStartTime) / 1000000000;
            elapsedTimeView.setText("Elapsed Time: " + elapsedTime + "s");
            usernameView.setText(username);
            usernameView.setVisibility(View.VISIBLE);
        }
    }

    private void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
        emailText.setEnabled(false);
        checkButton.setEnabled(false);
    }

    private void hideLoading() {
        progressBar.setVisibility(View.INVISIBLE);
        emailText.setEnabled(true);
        checkButton.setEnabled(true);
    }

    private void showMessage(String message) {
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP, 0, 125);
        toast.show();
    }

    /**
     * method is used for checking valid email id format.
     *
     * @param email
     * @return boolean true for valid false for invalid
     */
    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        CharSequence inputStr = email;

        Matcher matcher = Patterns.EMAIL_ADDRESS.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }
}
