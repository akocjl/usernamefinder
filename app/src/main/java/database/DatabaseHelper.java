package database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import database.DatabaseContract.AlphabetTable;


/**
 * Created by JL on 1/10/2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "usernames.db";
    private static DatabaseHelper instance;

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String DOUBLE_TYPE = " DOUBLE";
    private static final String DATE_TYPE = " DATE";
    private static final String BOOL_TYPE = " BOOL";
    private static final String PRIMARY_KEY = " PRIMARY KEY";
    private static final String COMMA_SEP = ",";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static DatabaseHelper getInstance(Context context) {

        if (instance == null)
            instance = new DatabaseHelper(context);

        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        // create the tables
        for (int i = 0; i < AlphabetTable.tableNames.length; i++) {
             createTable(AlphabetTable.tableNames[i], sqLiteDatabase);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        for (int index = 0; i< AlphabetTable.tableNames.length; i++) {
            dropTable(AlphabetTable.tableNames[index], sqLiteDatabase);
            createTable(AlphabetTable.tableNames[index], sqLiteDatabase);
        }
    }

    private void createTable(String name, SQLiteDatabase db) {
        String SQL_CREATE = "CREATE TABLE " + name + "("
                + AlphabetTable._ID + INTEGER_TYPE + PRIMARY_KEY + COMMA_SEP
                + AlphabetTable.COL_NAME + TEXT_TYPE +COMMA_SEP
                + AlphabetTable.COL_NUMBER + INTEGER_TYPE + ")";

        db.execSQL(SQL_CREATE);
    }

    private void dropTable(String name, SQLiteDatabase db) {
        String SQL_DELETE = "DROP TABLE IF EXISTS " + name;
        db.execSQL(SQL_DELETE);
    }
}
