package database;

import android.provider.BaseColumns;

/**
 * Created by JL on 1/10/2015.
 */
public class DatabaseContract {

    public static abstract class AlphabetTable implements BaseColumns {
        public static final String COL_NAME = "name";
        public static final String COL_NUMBER = "number";
        public static final String[] tableNames = { "a", "b", "c", "d", "e",
                                                    "f", "g", "h", "i", "j",
                                                    "k", "l", "m", "n", "o",
                                                    "p", "q", "r", "s", "t",
                                                    "u", "v", "w", "x", "y", "z"};
    }
}
