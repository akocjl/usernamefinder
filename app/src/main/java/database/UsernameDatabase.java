package database;

import android.content.Context;
import android.database.Cursor;

/**
 * Created by JL on 1/11/2015.
 */
public class UsernameDatabase {

    private DatabaseHelper mDatabaseHelper;

    public UsernameDatabase(Context context) {
        this.mDatabaseHelper = new DatabaseHelper(context);
    }

    public Cursor getUsernames(String name){
        String[] columns = { DatabaseContract.AlphabetTable.COL_NUMBER };
        String selection = DatabaseContract.AlphabetTable.COL_NAME + " = ?";
        String[] selectionArgs = new String[] { name };
        String tableName = String.valueOf(name.charAt(0));

        return mDatabaseHelper.getReadableDatabase().query(tableName, columns, selection, selectionArgs, null, null, null);
    }
}
