package helper;

import android.content.Context;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by JL on 1/10/2015.
 */
public class IO {

    public static boolean fileExists(Context context, String fileName) {
        File file = context.getFileStreamPath(fileName);
        return file.exists();
    }

    public static void copyDatabaseFromAssets(String fileName, Context context)
            throws IOException {
        String databasePath = context.getDatabasePath(fileName).getPath();

        InputStream is = null;
        OutputStream os = null;

        is = context.getAssets().open(fileName);
        os = new FileOutputStream(databasePath);

        // transfer bytes from the input file to the output file
        byte[] buffer = new byte[10240];
        int length;
        while ((length = is.read(buffer)) > 0)
        {
            os.write(buffer, 0, length);
        }
        // Close the streams
        os.flush();
        os.close();
        is.close();
    }
}
